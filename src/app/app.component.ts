import { Component } from '@angular/core';

@Component({
  selector: 'onayaz',
  template: `
    <!--<div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader"></div>
            <p>Authenticating...</p>
        </div>
    </div>-->
    <div class="wrapper" >
      <router-outlet></router-outlet>
    </div>
  `
})
export class AppComponent {
  // Constructor
  public constructor() { }
}
