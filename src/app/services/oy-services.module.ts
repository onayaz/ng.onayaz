import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

// Local
import { PeopleService } from './people';
import { MessageService } from './message';


@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        HttpClientModule
    ],
    declarations: [],
    exports: [],
    providers: [
        PeopleService,
        MessageService
    ]
})
export class OnayazServicesModule { }