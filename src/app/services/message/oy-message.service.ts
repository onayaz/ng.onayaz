import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

// Local
import { Message } from './message.model';

@Injectable()
export class MessageService {


    public constructor(private http: HttpClient) {

    }

    public fetchAllMessages(personId: string): Observable<Message[]> {
        return this.http.get<Message[]>("https://5cf2e8ecca993e001483b54d.mockapi.io/onayaz/people/"+personId+"/message");
    }
}