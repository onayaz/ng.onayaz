export interface Message {
    id: string;
    uniqueId: string;
    author: string;
    message: string;
    messagePlus: number;
    messageMinus: number;
    imgUrl: string;
}