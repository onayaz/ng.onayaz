export interface Person {
    id: string;
    uniqueId: string;
    familyName: string;
    givenName: string;
    imgUrl: string;
    title: string;
    description: string;
    numberOfMessages: number;
    numberOfGoodMessages: number;
    numberOfBadMessages: number;
    bestTextWritter: string;
    bestTextPlus: number;
    bestTextMinus: number;
}