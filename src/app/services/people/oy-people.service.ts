import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Person } from './people.model';

@Injectable()
export class PeopleService {

    public searchText: string = "";

    public constructor(
        private http: HttpClient
    ) { }

    public fetchAll(): Observable<Person[]> {
        return this.http.get<Person[]>("https://5cf2e8ecca993e001483b54d.mockapi.io/onayaz/people");
    }

    public fetchById(uniqueId: string): Observable<Person> {
        return this.http.get<Person>("https://5cf2e8ecca993e001483b54d.mockapi.io/onayaz/people/" + uniqueId);
    }

}