import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
import { CommonModule } from '@angular/common';


// Local

import { OnayazCoreModule } from "@onayaz/core";
import { OnayazSharedModule } from "@onayaz/shared";

import { AppComponent } from './app.component';
import { OnayazDashboardRootModule } from './oy.dashboard/oy-dashboard.root.module';
import { OnayazServicesModule } from './services/oy-services.module';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,

    OnayazCoreModule,
    OnayazSharedModule.forRoot(),

    OnayazServicesModule,
    
    OnayazDashboardRootModule,
    RouterModule.forRoot([
      {
        path: "",
        pathMatch: "full",
        redirectTo: "/dashboard"
      }
    ])
  ],
  declarations: [
    AppComponent,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
