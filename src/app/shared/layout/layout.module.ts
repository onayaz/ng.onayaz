import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from "@angular/forms";


// Local
import { HeaderComponent } from './header/header.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { MainSearchComponent } from './main-search/main-search.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule
    ],
    declarations: [
    
        HeaderComponent,
        MainLayoutComponent,
        MainSearchComponent
    ],
    providers: [],
    entryComponents: [
    
        HeaderComponent,
        MainLayoutComponent,
        MainSearchComponent
    ],
    exports: [
    
        HeaderComponent,
        MainLayoutComponent,
        MainSearchComponent
    ]
})
export class LayoutModule {}