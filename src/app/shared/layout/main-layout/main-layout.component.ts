import { Component } from "@angular/core";

@Component({
    selector: "main-layout",
    template:`
        <oy-header></oy-header>
        <div class="container main-wrapper">
            <oy-main-search></oy-main-search>
            <router-outlet></router-outlet>
        </div>
    `
})
export class MainLayoutComponent{}