import { Component } from '@angular/core';

// Onayaz
import { PeopleService } from '@onayaz/services/people';
import { Router } from '@angular/router';

@Component({
    selector: 'oy-main-search',
    templateUrl: 'main-search.component.html'
})
export class MainSearchComponent {

    public constructor(
        public peopleService: PeopleService,
        private router: Router) { }

    public navigateToHome() {
        this.router.navigateByUrl("");
    }
}