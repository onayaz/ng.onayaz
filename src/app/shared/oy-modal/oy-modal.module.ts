import { NgModule } from "@angular/core";
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

// Local
import { OyModalService } from './oy-modal.service';

@NgModule({
    imports: [
        NgbModalModule
    ],
    exports: [],
    declarations: [],
    providers: [
        OyModalService
    ]
})
export class OyModalModule {

}