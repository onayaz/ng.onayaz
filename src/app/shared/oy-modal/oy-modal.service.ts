import { Injectable } from "@angular/core";
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class OyModalService {
    public constructor(
        private modalService: NgbModal
    ) { }

    public open(content: any, options?: NgbModalOptions): NgbModalRef {
        if (!options) {
            options = {
                size: "lg",
                windowClass: "oy-modal"
            }
        }
        return this.modalService.open(content, options);
    }
}