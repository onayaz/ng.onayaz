import { Directive, Input, ElementRef } from "@angular/core";

//Third
import * as $ from "jquery";

// Declaration
declare var $: any;

@Directive({
    selector: "[oy-popover]",
})
export class OyPopoverDirective {

    // Input Variables
    @Input("oy-popover") bodyText: string | HTMLElement | Function = "";
    @Input("oy-popover-placement") placement: "auto" | "top" | "bottom" | "left" | "right" = "auto";
    @Input("oy-popover-trigger") trigger: "click" | "hover" | "focus" = "hover";

    private host: HTMLElement;

    // Construcotr
    constructor(public element: ElementRef) {
        this.host = element.nativeElement;
    }

    // On Int
    public ngOnInit(): void {
        this.generatePopover();
    }

    // Metehods
    private generatePopover() {
        $(this.host).popover({
            content: this.bodyText,
            trigger: this.trigger,
            placement: this.placement,
            html: true,
            template: this.getHtmlForPopover(),

        });
    }

    private getHtmlForPopover(): string {
        return `
            <div class="popover oy-popover" role="tooltip">
                <div class="arrow"></div>
                <div class="popover-body"></div>
            </div>
        `;
    }
}