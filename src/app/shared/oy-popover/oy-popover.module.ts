import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// Local
import { OyPopoverDirective } from "./directives/sm-popover.directive";


@NgModule({
    imports: [CommonModule],
    providers: [],
    declarations: [OyPopoverDirective],
    exports: [OyPopoverDirective]
})
export class OyPopoverModule {
    public constructor() { }
}