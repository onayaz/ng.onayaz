

export function newUUID() {
    // your favorite UUID generation function could go here
    // ex: http://stackoverflow.com/a/8809472/188246
    let time = new Date().getTime();
    if (window.performance && typeof window.performance.now === 'function') {
        time += performance.now(); // use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
        const r = ((time + Math.random() * 16) % 16) | 0;
        time = Math.floor(time / 16);
        return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16);
    });
}
/**
 * Check if the given test string is a valid uuid string.
 * https://stackoverflow.com/questions/7905929/how-to-test-valid-uuid-UUID/13653180#13653180
 * @param {string} uuidTestString
 * @returns {boolean} True if it is a valid uuid string, otherwise false.
 */
export function isValidUUID(uuid: string) {
    return /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(uuid);
}