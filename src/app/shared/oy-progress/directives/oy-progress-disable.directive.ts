import { Directive, ElementRef, Input, Optional, SkipSelf } from '@angular/core';

// Local
import { OyProgressManagerService } from '../services/oy-progress-manager.service';
import { OyProgressTracker } from '../services/oy-progress-tracker.service';
import { OyProgress } from '../models/oy-progress.model';
import { OyProgressTrackerInstance } from '../services/oy-progress-tracker-instance';
import { OyAbstractProgressDirective } from "./oy-abstract-progress.directive";


@Directive({
    selector: '[progress-disable]',
    providers: [OyProgressTrackerInstance]
})
export class OyProgressDisableDirective extends OyAbstractProgressDirective {

    // Members
    @Input("progress-disable") protected keyOrTracker: string | OyProgressTracker;

    // Constructor
    public constructor(
        private el: ElementRef,
        manager: OyProgressManagerService,
        trackerInstance: OyProgressTrackerInstance,
        @Optional() @SkipSelf() parentTrackerInstance: OyProgressTrackerInstance
    ) {
        super(manager, trackerInstance, parentTrackerInstance);
    }

    // Methods
    protected prepareElements() { }
    protected progressChanged(progress: OyProgress) {
        if (progress.isActive) {
            this.el.nativeElement.style.pointerEvents = "none";
            this.el.nativeElement.disabled = true;
        } else {
            this.el.nativeElement.style.pointerEvents = "auto";
            this.el.nativeElement.disabled = false;
        }
    }
}