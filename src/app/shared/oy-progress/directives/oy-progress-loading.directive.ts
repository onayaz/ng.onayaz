import { Directive, ElementRef, Input, Optional, SkipSelf, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

// Local
import { OyProgressManagerService } from '../services/oy-progress-manager.service';
import { OyProgressTracker } from '../services/oy-progress-tracker.service';
import { OyProgress } from '../models/oy-progress.model';
import { OyProgressTrackerInstance } from '../services/oy-progress-tracker-instance';
import { OyAbstractProgressDirective } from "./oy-abstract-progress.directive";



@Directive({
    selector: '[progress-loading]',
    providers: [OyProgressTrackerInstance]
})
export class OyProgressLoadingDirective extends OyAbstractProgressDirective {

    // Members
    @Input("progress-loading") protected keyOrTracker: string | OyProgressTracker;
    @Input("progress-theme") private theme: string;
    private hostElement: HTMLElement;
    private loadingBoxContainerElement: HTMLElement;
    private loadingSpinner: HTMLElement;
    private loadingSubject: HTMLElement;
    private loadingHint: HTMLElement;

    // Constructor
    public constructor(
        host: ElementRef,
        @Inject(DOCUMENT) private document: Document,
        manager: OyProgressManagerService,
        trackerInstance: OyProgressTrackerInstance,
        @Optional() @SkipSelf() parentTrackerInstance: OyProgressTrackerInstance
    ) {
        super(manager, trackerInstance, parentTrackerInstance);
        this.hostElement = host.nativeElement;
    }

    // Methods
    protected prepareElements() {
        this.createLoadingBarElement();
    }
    protected progressChanged(progress: OyProgress) {

        if (!this.loadingBoxContainerElement) return;

        if (progress.isActive) {
            this.loadingBoxContainerElement.classList.add("active")

            if (!this.theme || this.theme.trim().length == 0)
                this.theme = "default";

            this.loadingBoxContainerElement.classList.add("oy-progress-loader-" + this.theme);

            // Subject
            this.loadingSubject.innerHTML = progress.subject;
            if (progress.subject && progress.subject.trim().length > 0)
                this.loadingSubject.classList.add("oy-progress-loader-has-subject");
            else
                this.loadingSubject.classList.remove("oy-progress-loader-has-subject");


            // HINT
            this.loadingHint.innerHTML = progress.hint;
            if (progress.hint && progress.hint.trim().length > 0)
                this.loadingHint.classList.add("oy-progress-loader-has-hint");
            else
                this.loadingHint.classList.remove("oy-progress-loader-has-hint");


            this.loadingHint.innerText = progress.hint;
            this.loadingSubject.innerText = progress.subject;


        } else {
            this.loadingBoxContainerElement.classList.remove("active")
        }

        this.loadingSubject.style.display = (progress.subject && progress.subject.trim().length > 0) ? "block" : "none";
        this.loadingHint.style.display = (progress.hint && progress.hint.trim().length > 0) ? "block" : "none";

    }
    private createLoadingBarElement() {
        var container = this.document.createElement('div');
        container.innerHTML = `
            <div class="oy-progress oy-progress-loader">
                <div class="oy-progress-fb-spinner"> <div></div> <div></div> <div></div> </div>
                <div class="oy-progress-loader-subject"></div>
                <div class="oy-progress-loader-hint"></div>
            </div>
        `.trim();
        this.loadingBoxContainerElement = container.firstChild as HTMLElement;
        this.loadingSpinner = container.querySelector(".oy-progress-fb-spinner");
        this.loadingSubject = container.querySelector(".oy-progress-loader-subject");
        this.loadingHint = container.querySelector(".oy-progress-loader-hint");
        this.hostElement.insertBefore(this.loadingBoxContainerElement, this.hostElement.firstChild);
    }
}