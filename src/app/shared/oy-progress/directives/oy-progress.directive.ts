import { Directive, Input, Optional, SkipSelf } from '@angular/core';

// Local
import { OyProgressManagerService } from '../services/oy-progress-manager.service';
import { OyProgressTracker } from '../services/oy-progress-tracker.service';
import { OyProgress } from '../models/oy-progress.model';
import { OyProgressTrackerInstance } from '../services/oy-progress-tracker-instance';
import { OyAbstractProgressDirective } from "./oy-abstract-progress.directive";

@Directive({
    selector: '[progress]',
    providers: [OyProgressTrackerInstance]
})
export class OyProgressDirective extends OyAbstractProgressDirective {

    // Members
    @Input("progress") protected keyOrTracker: string | OyProgressTracker;


    // Constructor
    public constructor(
        manager: OyProgressManagerService,
        trackerInstance: OyProgressTrackerInstance,
        @Optional() @SkipSelf() parentTrackerInstance: OyProgressTrackerInstance
    ) {
        super(manager, trackerInstance, parentTrackerInstance);
    }

    // Methods
    protected prepareElements() {
    }
    protected progressChanged(progress: OyProgress) { }

}