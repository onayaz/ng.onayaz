import { OnInit, OnChanges, SimpleChanges, Optional, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";



// Local
import { OyProgressManagerService } from "../services/oy-progress-manager.service";
import { OyProgressTracker } from "../services/oy-progress-tracker.service";
import { OyProgressTrackerInstance } from "../services/oy-progress-tracker-instance";
import { OyProgress } from "../models/oy-progress.model";




export abstract class OyAbstractProgressDirective  implements OnInit, OnChanges, OnDestroy {
    // Members
    protected abstract keyOrTracker: string | OyProgressTracker;
    private tracker: OyProgressTracker;
    private subscription: Subscription;

    // Constructor
    public constructor(
        private manager: OyProgressManagerService,
        private trackerInstance: OyProgressTrackerInstance,
        private parentTrackerInstance: OyProgressTrackerInstance
    ) { }

    // Methods
    public ngOnInit(): void {

        // prepare elements for directive
        this.prepareElements();


        // configure directive to handle progress changes
        this.configure();
    }
    public ngOnChanges(changes: SimpleChanges) {
        if (changes.keyOrTracker) {
            this.configure();
        }
    }
    public ngOnDestroy() {
        if (this.subscription)
            this.subscription.unsubscribe();

    }
    private configure() {

        if (typeof (this.keyOrTracker) === "string") {

            let finalTrackerPath = this.keyOrTracker;

            if (finalTrackerPath.length == 0 && this.parentTrackerInstance == null)
                return;


            if (finalTrackerPath.length == 0) {
                finalTrackerPath = this.parentTrackerInstance.tracker.path;
            } else if (!this.keyOrTracker.startsWith("/"))
                finalTrackerPath = this.parentTrackerInstance.tracker.path + "/" + this.keyOrTracker;

            // try to get the process handler instance
            this.tracker =
                this.manager.get(finalTrackerPath);
        }
        else
            this.tracker = this.keyOrTracker;

        this.trackerInstance.tracker = this.tracker;

        // close the previuse subscription, which we will re subscribe again
        if (this.subscription)
            this.subscription.unsubscribe();


        // subscribe for changes
        this.subscription = this.tracker.progress.subscribe(progress => {
            this.progressChanged(progress);
        });
    }


    protected abstract prepareElements();
    protected abstract progressChanged(progress: OyProgress);

}