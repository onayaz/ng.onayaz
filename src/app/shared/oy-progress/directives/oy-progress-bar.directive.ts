import { Directive, ElementRef, Input, Optional, SkipSelf, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';


// Local
import { OyProgressManagerService } from '../services/oy-progress-manager.service';
import { OyProgressTracker } from '../services/oy-progress-tracker.service';
import { OyProgress } from '../models/oy-progress.model';
import { OyProgressTrackerInstance } from '../services/oy-progress-tracker-instance';
import { OyAbstractProgressDirective } from "./oy-abstract-progress.directive";



@Directive({
    selector: '[progress-bar]',
    providers: [OyProgressTrackerInstance]
})
export class OyProgressBarDirective extends OyAbstractProgressDirective {

    // Members
    @Input("progress-bar") protected keyOrTracker: string | OyProgressTracker;
    private hostElement: HTMLElement;
    private barContainerElement: HTMLElement;
    private barIndecatorElement: HTMLElement;



    // Constructor
    public constructor(
        host: ElementRef,
        @Inject(DOCUMENT) private document: Document,
        manager: OyProgressManagerService,
        trackerInstance: OyProgressTrackerInstance,
        @Optional() @SkipSelf() parentTrackerInstance: OyProgressTrackerInstance
    ) {
        super(manager, trackerInstance, parentTrackerInstance);
        this.hostElement = host.nativeElement;
    }

    // Methods
    protected prepareElements() {
        // change display type of nativeElem
        if (!this.hostElement.style.position)
            this.hostElement.style.position = "relative";

        // we need the bar element
        this.createLoadingBarElement();


    }
    protected progressChanged(progress: OyProgress) {

        if (!this.barContainerElement) return;

        if (!progress.isActive) {
            this.barContainerElement.classList.remove("active");
            return;
        }

        this.barContainerElement.classList.add("active");
        if (progress.indeterminate) {
            this.barContainerElement.classList.add("indeterminate");
            this.barIndecatorElement.style.width = "100%";
        }
        else {
            this.barContainerElement.classList.remove("indeterminate");
            this.barIndecatorElement.style.width = 100 * progress.value / progress.max + "%";
        }
    }


    // provide the html of the line as a Node
    private createLoadingBarElement() {
        var container = this.document.createElement('div');
        container.innerHTML = `<div class="oy-progress oy-progress-bar">
                <div class="oy-progress-indicator"></div>
            </div>`
            .trim();
        this.barContainerElement = container.firstChild as HTMLElement;
        this.barIndecatorElement = container.querySelector(".oy-progress-indicator");
        this.hostElement.insertBefore(this.barContainerElement, this.hostElement.firstChild);
    }


}