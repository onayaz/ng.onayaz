import { Directive, ElementRef, Input, Optional, SkipSelf, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

// Local
import { OyProgressManagerService } from '../services/oy-progress-manager.service';
import { OyProgressTracker } from '../services/oy-progress-tracker.service';
import { OyProgress } from '../models/oy-progress.model';
import { OyProgressTrackerInstance } from '../services/oy-progress-tracker-instance';
import { OyAbstractProgressDirective } from "./oy-abstract-progress.directive";



@Directive({
    selector: '[progress-button]',
    providers: [OyProgressTrackerInstance]
})
export class OyProgressButtonDirective extends OyAbstractProgressDirective {

    // Members
    @Input("progress-button") protected keyOrTracker: string | OyProgressTracker;
    private hostElement: HTMLElement;
    private buttonOverlayContainerElement: HTMLElement;

    // Constructor
    public constructor(
        host: ElementRef,
        @Inject(DOCUMENT) private document: Document,
        manager: OyProgressManagerService,
        trackerInstance: OyProgressTrackerInstance,
        @Optional() @SkipSelf() parentTrackerInstance: OyProgressTrackerInstance
    ) {
        super(manager, trackerInstance, parentTrackerInstance);
        this.hostElement = host.nativeElement;
    }


    // Methods
    protected prepareElements() {
        if (!this.hostElement.style.position)
            this.hostElement.style.position = "relative";

        this.createLoadingBarElement();
    }
    protected progressChanged(progress: OyProgress) {

        if (!this.buttonOverlayContainerElement) return;

        if (progress.isActive) {
            this.buttonOverlayContainerElement.classList.add("active")
        } else {
            this.buttonOverlayContainerElement.classList.remove("active")
        }
    }

    private createLoadingBarElement() {

        var container = this.document.createElement('div');
        container.innerHTML = `<div class="oy-progress oy-progress-button"><div class="oy-progress-fb-spinner"><div></div><div></div><div></div></div></div>`;
        this.buttonOverlayContainerElement = container.firstChild as HTMLElement;
        this.hostElement.insertBefore(this.buttonOverlayContainerElement, this.hostElement.firstChild);
    }
}