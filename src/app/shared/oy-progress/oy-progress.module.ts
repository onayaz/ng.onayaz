import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";

// Directives
import { OyProgressDirective } from "./directives/oy-progress.directive";
import { OyProgressLoadingDirective } from "./directives/oy-progress-loading.directive";
import { OyProgressDisableDirective } from "./directives/oy-progress-disable.directive";
import { OyProgressBarDirective } from "./directives/oy-progress-bar.directive";

// Services
import { OyProgressManagerService } from "./services/oy-progress-manager.service";
import { OyProgressButtonDirective } from "./directives/oy-progress-button.directive";

// Components
import { OyProgressSpinnerComponent } from "./components/oy-progress-spinner.component";

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
  ],
  declarations: [
    OyProgressDirective,
    OyProgressLoadingDirective,
    OyProgressDisableDirective,
    OyProgressBarDirective,
    OyProgressButtonDirective,

    OyProgressSpinnerComponent
  ],
  exports: [
    OyProgressDirective,
    OyProgressLoadingDirective,
    OyProgressDisableDirective,
    OyProgressBarDirective,
    OyProgressButtonDirective,

    OyProgressSpinnerComponent
  ]
})
export class OyProgressModule {

  // Constructor
  public constructor() {
  }

  // Static Methods
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: OyProgressModule,
      providers: [OyProgressManagerService],
    };
  }
}