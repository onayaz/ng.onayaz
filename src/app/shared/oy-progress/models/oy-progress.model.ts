export interface OyProgress {
  isActive: boolean;
  isBusy: boolean;
  value: number;
  max: number;
  indeterminate: boolean;
  subject: string;
  hint: string;
  last_updated?: Date;
}
