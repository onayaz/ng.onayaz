import { Component } from "@angular/core";


@Component({
    selector: "oy-progress-spinner",
    template: 
    `
        <div class="oy-progress"><div class="oy-progress-fb-spinner"><div></div><div></div><div></div></div></div>
    `
})
export class OyProgressSpinnerComponent { 
    
}