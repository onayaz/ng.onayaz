import { Injectable } from "@angular/core";
import { OyProgressTracker } from "./oy-progress-tracker.service";
import { OyProgressManagerService } from "./oy-progress-manager.service";

@Injectable()
export class OyProgressTrackerInstance {

    // Members
    public tracker: OyProgressTracker;

    // Constructor
    public constructor(private manager: OyProgressManagerService) { }

    // Methods
    public get(key: string) {
        return this.manager.get(this.tracker.path + "/" + key);
    }
}