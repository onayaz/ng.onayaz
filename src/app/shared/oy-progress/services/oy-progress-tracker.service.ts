import { BehaviorSubject } from "rxjs";
import { OyProgress } from "../models/oy-progress.model";

interface OyProgressUpdateOptions {
  value?: number,
  max?: number,
  indetermined?: boolean;
  subject?: string;
  hint?: string;
}

export class OyProgressTracker {

  // Members
  public path: string;
  public parent?: OyProgressTracker;
  public children: OyProgressTracker[];
  public progress: BehaviorSubject<OyProgress>;


  // Constructor
  public constructor(path: string)
  public constructor(path: string, parent: OyProgressTracker)
  public constructor(path: string, parent?: OyProgressTracker) {

    // initiate default values
    this.progress = new BehaviorSubject({
      isActive: false,
      isBusy: false,
      value: 0,
      max: 0,
      indeterminate: false,
      subject: null,
      hint: null,
    });


    this.path = path;
    this.parent = parent;
    this.children = [];

    // subscribe progress change
    this.progress.subscribe(progress => {
      if (this.parent)
        this.parent.internalCalculateProgress();
    })

  }

  // Methods
  public start();
  public start(subject: string);
  public start(subject: string, hint: string);
  public start(subject?: string, hint?: string) {
    this.update({
      indetermined: true,
      subject: subject,
      hint: hint
    });
  }
  public update(options?: OyProgressUpdateOptions) {

    if (options) {
      if (options.value != undefined)
        this.progress.value.value = options.value;

      if (options.max != undefined)
        this.progress.value.max = options.max;

      if (options.indetermined != undefined)
        this.progress.value.indeterminate = options.indetermined;

      if (options.subject != undefined)
        this.progress.value.subject = options.subject;
      if (options.hint != undefined)
        this.progress.value.hint = options.hint;
    }
    this.progress.value.isBusy = true;
    this.progress.next(this.progress.value);

    this.internalCalculateProgress()

  }
  public complete(): void {
    this.progress.value.value = 0;
    this.progress.value.max = 0;
    this.progress.value.indeterminate = false;
    this.progress.value.subject = null;
    this.progress.value.hint = null;
    this.progress.value.isBusy = false;

    this.internalCalculateProgress();
  }


  public internalCalculateProgress() {

    let finalProgress = 0;
    let finalProgressMax = 0;
    let isActive = this.progress.value.isBusy;

    if (isActive) {
      finalProgress += this.progress.value.value;
      finalProgressMax += this.progress.value.max;
    }

    this.children.forEach(child => {
      if (child.progress.value.isActive) {
        finalProgress += child.progress.value.value;
        finalProgressMax += child.progress.value.max;
        isActive = true;
      }
    });

    let newProgress: OyProgress = {
      isActive: isActive,
      isBusy: this.progress.value.isBusy,
      last_updated: new Date(Date.now()),
      value: finalProgress,
      max: finalProgressMax,
      indeterminate: this.progress.value.indeterminate,
      subject: this.progress.value.subject,
      hint: this.progress.value.hint
    };

    // next we need to update the gloal version
    this.progress.next(newProgress);

  }
}
