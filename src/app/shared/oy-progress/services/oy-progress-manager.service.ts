import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";


// Local
import { OyProgressTracker } from "./oy-progress-tracker.service";


export type OyProgressTrackersCollection = { [key: string]: OyProgressTracker }


@Injectable()
export class OyProgressManagerService {

  // Variables
  public trackers: BehaviorSubject<OyProgressTrackersCollection>;
  public rootTracker: OyProgressTracker;

  // Constructor
  public constructor() {

    // list of all progress ( active / deactive )
    this.trackers = new BehaviorSubject({});

    // we need to have the root parent for all processes
    this.rootTracker = new OyProgressTracker("/");
    this.trackers.value["/"] = this.rootTracker;
  }


  // Methods
  public get(path: string): OyProgressTracker {

    if (path == null) console.error("InvalidPathError "+path);
    path = path.replace(/\/\//g, '$1/'); // to ignore '//' sometime it happenes 
    path = path.replace(/\/$/g, '$1'); // to ignore '//' sometime it happenes 

    let process = this.buildByPath(path);
    return process;
  }


  private buildByPath(path: string): OyProgressTracker {

    if (path == null || !path.startsWith("/"))
    console.error("InvalidPathError "+path);

    let paths = path.split('/');

    let parentPath: OyProgressTracker = this.rootTracker;

    paths.forEach((p, i) => {

      // empty path means its root so we need to change it to '/'
      if (p == "") {
        return;
      }

      let fullPath = ((parentPath.path == "/") ? "" : parentPath.path) + "/" + p;

      let childProcess = this.trackers.value[fullPath]

      if (childProcess == null) {

        childProcess = new OyProgressTracker(
          fullPath,
          parentPath);

        // we need to push into the parent
        //    and also we need to push into global processes list which is flat and lookingup for a process it's quicker than hierarchy
        parentPath.children.push(childProcess);
        this.trackers.value[childProcess.path] = childProcess;
      }

      parentPath = childProcess;
    });


    return this.trackers.value[path];
  }
}