import { NgModule, ModuleWithProviders, Optional, SkipSelf } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

// Local
import { OyProgressModule } from './oy-progress';
import { LayoutModule } from './layout';
import { OyPipesModule } from './oy-pipes';
import { OyPopoverModule } from './oy-popover';
import { OyModalModule } from './oy-modal';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,

        // Modules
        OyProgressModule,
        LayoutModule,
        OyPipesModule,
        OyPopoverModule,
        OyModalModule


    ],
    exports: [
        // Modules
        OyProgressModule,
        LayoutModule,
        OyPipesModule,
        OyPopoverModule,
        OyModalModule

    ]
})
export class OnayazSharedModule {

    public static forRoot(): ModuleWithProviders {
        return {
            ngModule: OnayazSharedRootModule,
            providers: [],
        };
    }

}

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        OyProgressModule.forRoot(),
        LayoutModule
    ],
    declarations: [
    ],
    exports: [
        LayoutModule
    ]
})
export class OnayazSharedRootModule {

    // Constructor
    public constructor(
        @Optional() @SkipSelf() parentModule: OnayazSharedRootModule) {
        if (parentModule) {
            console.log("ModuleAlreadyLoadedError(" + OnayazSharedRootModule.name);
        }
    }

}
