import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
  name: "oyFilterPipe"
})
export class OyFilterPipe implements PipeTransform {
  transform(items: any[], searchText: string, props?: string[]) {
    let checkProps = function (value: any) {
      let result = false;
      props.forEach(prop => {
        let r = value[prop] && value[prop].toLowerCase().indexOf(searchText.toLowerCase()) > -1 && true;
        if (r)
          result = true;
      });
      return result;
    };
    if (searchText) {
      return items.filter(
        x =>
          (typeof x == "string" &&
            x.toLowerCase().indexOf(searchText.toLowerCase()) > -1) ||
          (x.name &&
            x.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1) ||
          (x.tags &&
            x.tags.findIndex(a => {
              return a.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
            }) > -1) ||
          (typeof x != "string" && props.length > 0 && checkProps(x))
      );
    } else {
      return items;
    }
  }
}
