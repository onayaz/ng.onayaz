import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

// LOCAL
import { OyFilterPipe } from './filter/oy-filter.pipe';
import { OyIsEmptyPipe } from './isEmpty/oy-is-empty.pipe';



@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        OyFilterPipe,
        OyIsEmptyPipe
    ],
    exports: [
        OyFilterPipe,
        OyIsEmptyPipe
    ]
})
export class OyPipesModule {

}
