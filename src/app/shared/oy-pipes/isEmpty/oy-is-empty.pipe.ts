import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "oyIsEmptyPipe"
})

export class OyIsEmptyPipe implements PipeTransform {
    transform(items: any[]) {
        if (items.length === 0)
            return true;

        return false;
    };
};
