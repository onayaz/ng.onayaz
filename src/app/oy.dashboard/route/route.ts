import { Route } from '@angular/router';

// Onayaz
import { MainLayoutComponent } from '@onayaz/shared/layout';
// Local
import { OnayazDashboardPage } from '../page/dashboard/oy-dashboard.page';
import { OnayazPersonPage } from '../page/person/oy-person.page';

export const route: Route[] = [
    {
        path: "",
        component: MainLayoutComponent,
        children: [
            {
                path: "",
                component: OnayazDashboardPage
            },
            {
                path: "person/:id",
                component: OnayazPersonPage
            }]
    }
]