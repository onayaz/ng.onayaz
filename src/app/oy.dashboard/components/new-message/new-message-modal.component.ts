import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    templateUrl: "./new-message-modal.component.html"
})
export class NewMessageModalComponent {


    public constructor(public modal: NgbActiveModal) { }
}