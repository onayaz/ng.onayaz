import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Person } from '@onayaz/services/people';

@Component({
    templateUrl: "./new-person-modal.component.html"
})
export class NewPersonModalComponent {
    @Input() public newPerson: Person;

    public constructor(public modal: NgbActiveModal) { }
}