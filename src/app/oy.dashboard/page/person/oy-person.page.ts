import { Component, OnInit, AfterViewInit, AfterContentInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { switchMap, tap, flatMap, map } from 'rxjs/operators';
import * as $ from "jquery";

// Onayaz
import { PeopleService, Person } from '@onayaz/services/people';
import { Message, MessageService } from '@onayaz/services/message';
import { OyProgressTracker, OyProgressManagerService } from '@onayaz/shared/oy-progress';
import { OyModalService } from '@onayaz/shared/oy-modal/oy-modal.service';
import { NewMessageModalComponent } from '../../components/new-message/new-message-modal.component';

declare var $: any;

@Component({
    templateUrl: "./oy-person.page.html"
})
export class OnayazPersonPage implements OnInit {

    public person: Person;
    public messages: Message[];
    public message: string = "";
    public tracker: OyProgressTracker;
    public focusedId: string = "";

    public constructor(
        private route: ActivatedRoute,
        private peopleService: PeopleService,
        private messageService: MessageService,
        private trackerManager: OyProgressManagerService,
        private modalService: OyModalService
    ) { }

    public ngOnInit(): void {
        this.tracker = this.trackerManager.get("/person");

        this.getItems();
    }

    private getItems(): void {
        this.tracker.start();
        this.route.paramMap.pipe(
            tap(paramMap => this.route.fragment.subscribe(id => this.focusedId = id)),
            flatMap(paramMap => this.fetchPerson(paramMap.get('id'))),
            flatMap(user => this.fetchMessages(user.id))
        ).subscribe(
            obs => {
                this.tracker.complete();
                setTimeout(() => {
                    if (this.focusedId && this.focusedId != "")
                        this.scrollToFocus();
                });
            },
            err => {
                this.tracker.complete();
            }
        )


    }
    private scrollToFocus() {
        document.getElementById(this.focusedId).scrollIntoView({ behavior: "smooth", });
    }

    public fetchPerson(uniqueId: string) {
        return this.peopleService.fetchById(uniqueId).pipe(
            map(person => this.person = person)
        );
    }
    public fetchMessages(uniqueId: string) {
        return this.messageService.fetchAllMessages(uniqueId).pipe(
            map(messages => this.messages = messages)
        );
    }

    public sendMessage() {
        this.modalService.open(NewMessageModalComponent, { size: "lg" }).result.then(
            result => {

            },
            reason => {

            }
        )
    }
}