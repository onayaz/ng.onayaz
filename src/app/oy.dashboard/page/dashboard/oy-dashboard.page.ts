import { Component, OnInit } from "@angular/core";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

// Onayaz
import { PeopleService, Person } from '@onayaz/services/people';
import { Router } from '@angular/router';
import { OyProgressTracker, OyProgressManagerService } from '@onayaz/shared/oy-progress';
import { OyModalService } from '@onayaz/shared/oy-modal/oy-modal.service';
import { NewPersonModalComponent } from '../../components/new-person/new-person-modal.component';
import { newUUID } from '@onayaz/shared/oy-utils';

@Component({
    templateUrl: "./oy-dashboard.page.html"
})
export class OnayazDashboardPage implements OnInit {

    public people: Person[] = [];
    public tracker: OyProgressTracker;

    public constructor(
        public peopleService: PeopleService,
        private trackerService: OyProgressManagerService,
        private router: Router,
        private modalService: OyModalService
    ) {

    }


    public doFetchPeople() {
        this.tracker.start();
        this.fetchPeople().subscribe(
            obs => { this.tracker.complete(); },
            err => { this.tracker.complete(); }
        )
    }

    public openAddNewPersonModal() {
        let newPersonRef = this.modalService.open(NewPersonModalComponent);
        newPersonRef.componentInstance.newPerson = <Person>{
            id: newUUID(),
            uniqueId: newUUID(),
            givenName: this.peopleService.searchText,
            familyName: ""
        }
        newPersonRef.result.then(
            success => {

            },
            err => {

            }
        )

    }

    private fetchPeople(): Observable<Person[]> {
        return this.peopleService.fetchAll()
            .pipe(map(people => this.people = people));
    }

    public navigateTo(url: string) {
        this.router.navigateByUrl(url);
    }

    public ngOnInit(): void {
        this.tracker = this.trackerService.get("/dashboard");
        this.doFetchPeople();

    }
}