import { NgModule, Optional, SkipSelf } from "@angular/core";
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Onayaz
import { OnayazSharedModule } from '@onayaz/shared';

// Local
import { OnayazDashboardPage } from './page/dashboard/oy-dashboard.page';
import { OnayazPersonPage } from './page/person/oy-person.page';
import { route } from './route/route';
import { FormsModule } from '@angular/forms';
import { NewMessageModalComponent } from './components/new-message/new-message-modal.component';
import { NewPersonModalComponent } from './components/new-person/new-person-modal.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(route),
        OnayazSharedModule
    ],
    declarations: [
        OnayazDashboardPage,
        OnayazPersonPage,
        NewMessageModalComponent,
        NewPersonModalComponent
    ],
    exports: [
        OnayazDashboardPage,
        OnayazPersonPage
    ],
    entryComponents:[ 
        NewMessageModalComponent,
        NewPersonModalComponent
    ]
})
export class OnayazDashboardModule {
    // Constructor
    public constructor(
        @Optional() @SkipSelf() parentModule: OnayazDashboardModule) {
        if (parentModule) {
            throw new Error(" Already Loaded !? " + OnayazDashboardModule.name);
        }
    }
}