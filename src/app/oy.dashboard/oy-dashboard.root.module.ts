import { NgModule, Optional, SkipSelf } from "@angular/core";
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// Onayaz
import { OnayazSharedModule } from '@onayaz/shared';

// Local

@NgModule({
    imports: [
        OnayazSharedModule,
        RouterModule.forRoot([{
            path: "dashboard",
            loadChildren: () => import('./oy-dashboard.lazy.module').then(m => m.OnayazDashboardModule)
        }])
    ]
})
export class OnayazDashboardRootModule { 
    constructor(@Optional() @SkipSelf() parentModule: OnayazDashboardRootModule) {
        if (parentModule) {
            throw new Error(" Already Loaded !? "+OnayazDashboardRootModule.name);
        }
    }
}